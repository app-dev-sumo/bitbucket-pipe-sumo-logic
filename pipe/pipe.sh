#!/usr/bin/env bash
#
# Sumo Logic Capture event
#

#source /common.sh
source "$(dirname "$0")/common.sh"

info "Executing bitbucket-pipes-sumo-logic-capture-event..."

# Required parameters
SUMO_LOGIC_BASE_URL=${SUMO_LOGIC_BASE_URL:?'SUMO_LOGIC_BASE_URL variable missing.'}
#SUMO_LOGIC_API_TOKEN=${SUMO_LOGIC_API_TOKEN:?'SUMO_LOGIC_API_TOKEN variable missing.'}

# Standard Bitbucket parameters
# https://confluence.atlassian.com/bitbucket/variables-in-pipelines-794502608.html
BITBUCKET_BUILD_NUMBER=${BITBUCKET_BUILD_NUMBER:="$BITBUCKET_BUILD_NUMBER"}
BITBUCKET_DEPLOYMENT_ENVIRONMENT=${BITBUCKET_DEPLOYMENT_ENVIRONMENT:="$BITBUCKET_DEPLOYMENT_ENVIRONMENT"}
BITBUCKET_GIT_HTTP_ORIGIN=${BITBUCKET_GIT_HTTP_ORIGIN:="$BITBUCKET_GIT_HTTP_ORIGIN"}
BITBUCKET_COMMIT=${BITBUCKET_COMMIT:="$BITBUCKET_COMMIT"}
BITBUCKET_BRANCH=${BITBUCKET_BRANCH:="$BITBUCKET_BRANCH"}
BITBUCKET_TAG=${BITBUCKET_TAG:="$BITBUCKET_TAG"}
BITBUCKET_PR_DESTINATION_BRANCH=${BITBUCKET_PR_DESTINATION_BRANCH:="$BITBUCKET_PR_DESTINATION_BRANCH"}
BITBUCKET_PROJECT_KEY=${BITBUCKET_PROJECT_KEY:="$BITBUCKET_PROJECT_KEY"}
BITBUCKET_REPO_FULL_NAME=${BITBUCKET_REPO_FULL_NAME:="$BITBUCKET_REPO_FULL_NAME"}
BITBUCKET_REPO_OWNER=${BITBUCKET_REPO_OWNER:="$BITBUCKET_REPO_OWNER"}
BITBUCKET_PR_ID=${BITBUCKET_PR_ID:="$BITBUCKET_PR_ID"}
BITBUCKET_EXIT_CODE=${BITBUCKET_EXIT_CODE:="$BITBUCKET_EXIT_CODE"}


# Optional parameters
DEBUG=${DEBUG:="false"}

# Calculated values
SUMO_LOGIC_API_URL="$SUMO_LOGIC_BASE_URL"
BITBUCKET_PIPELINES_LINK="https://bitbucket.org/${BITBUCKET_REPO_FULL_NAME}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"
BITBUCKET_COMMIT_LINK="https://bitbucket.org/${BITBUCKET_REPO_FULL_NAME}/commits/${BITBUCKET_COMMIT}"
CURRENT_DATE=`date +"%Y-%m-%d %T"`



DATA=$(cat <<EOF
{ \
  "buildNumber": "$BITBUCKET_BUILD_NUMBER", \
  "deploymentEnvironment": "$BITBUCKET_DEPLOYMENT_ENVIRONMENT", \
  "gitHttpOrigin": "$BITBUCKET_GIT_HTTP_ORIGIN", \
  "commit": "$BITBUCKET_COMMIT", \
  "branch": "$BITBUCKET_BRANCH", \
  "tag": "$BITBUCKET_TAG", \
  "prDestinationBranch": "$BITBUCKET_PR_DESTINATION_BRANCH", \
  "projectKey": "$BITBUCKET_PROJECT_KEY", \
  "repoFullName": "$BITBUCKET_REPO_FULL_NAME", \
  "repoOwner": "$BITBUCKET_REPO_OWNER", \
  "pr_id": "$BITBUCKET_PR_ID", \
  "pipe_result_link": "$BITBUCKET_PIPELINES_LINK", \
  "deploy_status": "$BITBUCKET_EXIT_CODE", \
  "commit_link": "$BITBUCKET_COMMIT_LINK", \
  "event_date": "$CURRENT_DATE" \
}
EOF
)

ARGS=(
  --request POST
  --url "${SUMO_LOGIC_API_URL}"
  --header "Content-type: application/json"
  --data "${DATA}"
)




if [[ "${DEBUG}" == "true" ]]; then
  info "Enabling debug mode."
  info "SUMO_LOGIC_API_URL = $SUMO_LOGIC_API_URL"
  info "$DATA"
  ARGS+=( --verbose )
fi

if curl "${ARGS[@]}"; then
  success "Success: Captured Sumo Logic event"
else
  fail "Error: Failed sending Sumo Logic event"
fi
