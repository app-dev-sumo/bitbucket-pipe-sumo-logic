#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/bitbucket-pipes-sumo-logic-capture-event"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE} .
}

@test "Test message is sent successfully" {
    run docker run \
        -e NAME="Pipelines is awesome!" \
        -e BITBUCKET_WORKSPACE="$BITBUCKET_WORKSPACE" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test message containing double quotes is sent successfully" {
    run docker run \
        -e NAME='"Pipelines is awesome!"' \
        -e BITBUCKET_WORKSPACE="$BITBUCKET_WORKSPACE" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

@test "Test debug flag" {
    run docker run \
        -e NAME="Pipelines is awesome!" \
        -e DEBUG="true" \
        -e BITBUCKET_WORKSPACE="$BITBUCKET_WORKSPACE" \
        -e BITBUCKET_REPO_SLUG="$BITBUCKET_REPO_SLUG" \
        -e BITBUCKET_BUILD_NUMBER="$BITBUCKET_BUILD_NUMBER" \
        -v $(pwd):$(pwd) \
        -v $(pwd)/tmp:/tmp \
        -w $(pwd) \
        ${DOCKER_IMAGE}

    echo "Status: $status"
    echo "Output: $output"

    [[ "$status" -eq 0 ]]
    [[ "$output" =~ "Enabling debug mode." ]]
}
