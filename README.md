# Bitbucket Pipelines Pipe: Sumo Logic Capture event
Capture event for Sumo Logic

## YAML Definition
Add the following snippet to the step section of your deployment part `bitbucket-pipelines.yml` file:

```yaml
   after-script:
          - pipe: docker://appdevsumologic/sumologic-send-event:1.15
            variables:
              SUMO_LOGIC_BASE_URL: '<SUMOLOGIC_HTTP_URL>'
```

## Variables

[TODO]: # (Document variables required by the pipe.)

| Variable | Usage                                                            |
| -------- | ---------------------------------------------------------------- |
| SOURCE   | Optional path to local directory. Default `${BITBUCKET_CLONE_DIR}` |
| DEBUG    | Turn on extra debug information. Default: `false`                |

_(*) = required variable._

## Details

[TODO]: # (Explain the Bitbucket pipe in more detail.)

Capture event for Sumo Logic

## Prerequisites

[TODO]: # (Document prerequisites for the pipe.)

## Examples
Basic example:

```yaml
after-script:
          - pipe: docker://appdevsumologic/sumologic-send-event:1.15
            variables:
              SUMO_LOGIC_BASE_URL: '<SUMOLOGIC_HTTP_URL>'
```

## Support
If you'd like help with this pipe,
or you have an issue or feature request,
[let us know on Community][community].

If you're reporting an issue,
please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
[Apache 2.0](LICENSE) allows
this software to be freely used, modified, and shared,
as long as you include the required notices.
This permissive license contains
a patent license from the contributors of the code
that allows modifications and larger works
to be distributed under different terms
and without source code.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes
