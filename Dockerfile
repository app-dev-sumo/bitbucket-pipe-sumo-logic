FROM alpine:3.10.3

ARG BITBUCKET_COMMIT="0"
ARG PIPE_VERSION="1.0.0"
ARG PIPE_BUILD_DATE="2020-02-20"


RUN apk add --update --no-cache \
  bash \
  curl \
  jq


ADD https://bitbucket.org/bitbucketpipelines/bitbucket-pipes-toolkit-bash/raw/0.5.0/common.sh /
COPY pipe /
COPY LICENSE AUTHORS README.md pipe.yml /

WORKDIR /opt/atlassian/bitbucketci/agent/build
ENTRYPOINT ["/pipe.sh"]
